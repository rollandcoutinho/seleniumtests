﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestFramework
{
    public static class Browser
    {
        static IWebDriver webDriver = new ChromeDriver(@"C:\temp\WebDriverDemo\WebDriverDemo\bin\Debug");

        public static void getDriver()
        {
            webDriver = new ChromeDriver(@"C:\temp\WebDriverDemo\WebDriverDemo\bin\Debug");
        }
        
        public static string Title
        {
            get => webDriver.Title;
            //internal set;
        }

        internal static void GoTo(string url)
        {
            webDriver.Url = url;
        }

        public static void Close()
        {
            webDriver.Close();
        }

        public static bool IsProximus(string application)
        {
            if (application.Equals(Pages.ProximusLoginPage.application))
                return true;
            else
                return false;
        }

        public static bool IsDoctorsApp(string application)
        {
            if (application.Equals(Pages.DoctorsAppLoginPage.application))
                return true;
            else
                return false;
        }

        public static bool IsChartViewer(string application)
        {
            if (application.Equals(Pages.ChartViewerLoginPage.application))
                return true;
            else
                return false;
        }

        internal static bool Login(string userNameValue, string passwordValue, bool expectedOutcome, string expectedMessage, string application)
        {
            IWebElement submit = null, login = null, message = null;

            var userNameField = webDriver.FindElement(By.Id("UserName"));
            userNameField.SendKeys(userNameValue);

            var passwordField = webDriver.FindElement(By.Id("Password"));
            passwordField.SendKeys(passwordValue);

            AppendToFile("User Name = " + userNameValue);
            AppendToFile("Password  = " + passwordValue);
            AppendToFile("Application = " + application);
            AppendToFile("Exp Msg = " + expectedMessage);

            if (IsProximus(application))
            {
                submit = webDriver.FindElement(By.TagName("button"));
                AppendToFile("In Proximus...");
            }
            else if (IsDoctorsApp(application))
            {
                submit = webDriver.FindElement(By.Id("btnLogin"));
                AppendToFile("In Doctors App...");
            }
            else if (IsChartViewer(application))
            {
                submit = webDriver.FindElement(By.Id("btnLogin"));
                AppendToFile("In Chart Viewer...");
            }
            sleep(1);
            submit.Click();
            sleep(1);

            // Testing for a Login success.
            if (expectedOutcome == true)
            {
                // Check if greeting check should be ignored.
                if (expectedMessage.Equals(""))
                {
                    return true;
                }
                else
                {
                    if (IsProximus(application))
                    {
                        login = webDriver.FindElement(By.Id("loginPartialContainer"));
                        AppendToFile("Actual Message = " + login.Text);
                        AppendToFile("Expected Message = " + expectedMessage);
                        AppendToFile("Returned Value = " + login.Text.Contains(expectedMessage));
                        return login.Text.Contains(expectedMessage);
                    }
                    else if (IsDoctorsApp(application))
                    {
                        login = webDriver.FindElement(By.Id("Banner1_BannerMessage"));
                        AppendToFile("Actual Message = " + login.Text);
                        AppendToFile("Expected Message = " + expectedMessage);
                        AppendToFile("Returned Value = " + login.Text.Contains(expectedMessage));
                        return login.Text.Contains(expectedMessage);
                    }
                    else if (IsChartViewer(application))
                    {
                        login = getChartViewerObject(login);
                        AppendToFile("Actual Message = " + login.Text);
                        AppendToFile("Expected Message = " + expectedMessage);
                        AppendToFile("Returned Value = " + login.Text.Contains(expectedMessage));
                        return login.Text.Contains(expectedMessage);
                    }
                    else
                        return false;
                }
            }

            // Testing for a Login failure.
            else
            {
                if (IsProximus(application))
                {
                    Browser.sleep(1);

                    message = webDriver.FindElement(By.ClassName("validation-summary-errors"));
                    return message.Text.Contains(expectedMessage);
                }
                else if (IsDoctorsApp(application))
                {
                    message = webDriver.FindElement(By.ClassName("ErrorMessage"));
                    return message.Text.Contains(expectedMessage);
                }
                else if (IsChartViewer(application))
                {
                    message = webDriver.FindElement(By.ClassName("ErrorMessage"));
                    AppendToFile("Actual Message = " + message.Text);
                    AppendToFile("Expected Message = " + expectedMessage);
                    AppendToFile("Returned Value = " + message.Text.Contains(expectedMessage));
                    return message.Text.Contains(expectedMessage);
                }
                else
                    return false;
            }
        }

        private static IWebElement getChartViewerObject(IWebElement element1)
        {
            try
            {
                // Regular ChartViewer Login Page
                element1 = webDriver.FindElement(By.Id("ctl00_BannerMessage"));
            }
            catch (OpenQA.Selenium.NoSuchElementException)
            {
                // "Change Password" ChartViewer Login Page
                element1 = webDriver.FindElement(By.Id("Banner1_BannerMessage"));
            }

            return element1;
        }

        internal static bool AccessDenied(string application)
        {
            if (IsDoctorsApp(application))
            {
                var message = webDriver.FindElement(By.ClassName("ErrorMessage"));
                if (message.Text.Contains("You don't have access to the Doctor's Application."))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        internal static bool FindTab(string expectedTabName, string expectedSelection)
        {
            string menuClass = "";
            if (expectedSelection.Equals("Unselected"))
                menuClass = "MenuUnSelected";
            else
                menuClass = "MenuSelected";

            bool tabFound = false;
            var selectedTabs = webDriver.FindElements(By.ClassName(menuClass));
            for (int i = 0; (i < selectedTabs.Count && tabFound == false); i++)
            {
                var tab = selectedTabs[i];
                AppendToFile("i = " + i + "; Selected Tab = " + tab.Text + "; Menu Class = " + menuClass);

                string tabName = tab.Text;
                if (tabName.Contains(expectedTabName))
                    tabFound = true;
            }
            AppendToFile("Tab Found = " + tabFound);
            return tabFound;
        }

        internal static bool Logout( string application )
        {
            if (IsProximus(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                logoutLink.Click();
                Browser.sleep(2);

                // Return length of Login Header.
                var element = webDriver.FindElement(By.ClassName("form-signin-heading"));
                return (element.Text.Length > 0);
            }
            else if (IsDoctorsApp(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                logoutLink.Click();
                Browser.sleep(2);

                // Return length of Credentials Header: "Enter Your Persante SleepCare Center's Doctor Application Login"
                var element = webDriver.FindElement(By.ClassName("SmallHeader"));
                return (element.Text.Length > 0);
            }
            else if (IsChartViewer(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                logoutLink.Click();
                Browser.sleep(2);

                // Return length of Credentials Header: "Enter Your Persante SleepCare Center's Doctor Application Login"
                var element = webDriver.FindElement(By.ClassName("SmallHeader"));
                return (element.Text.Length > 0);
            }
            else
                return false;
        }

        internal static bool ResetPassword(string expectedPageHeader, string expectedSubHeader, string application)
        {
            if (IsProximus(application))
            {
                IWebElement pageHeader = webDriver.FindElement(By.TagName("h1"));
                IWebElement subHeader = webDriver.FindElement(By.TagName("h3"));

                if (pageHeader.Text.Contains(expectedPageHeader) && (subHeader.Text.Contains(expectedSubHeader)))
                    return true;
                else
                    return false;
            }
            else if (IsDoctorsApp(application))
            {
                // Header should be "Enter Your New Sleepcare Password"
                IWebElement pageHeader = webDriver.FindElement(By.ClassName("SmallHeader"));

                if (pageHeader.Text.Contains(expectedPageHeader))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        internal static bool LogoutExists(string application)
        {
            if (IsProximus(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                AppendToFile("Logout Link = " + logoutLink.Text);
                AppendToFile("Returned Value = " + logoutLink.Text.Contains("Log off"));
                return logoutLink.Text.Contains("Log off");
            }
            else if (IsDoctorsApp(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                AppendToFile("Logout Link = " + logoutLink.Text);
                AppendToFile("Returned Value = " + logoutLink.Text.Contains("Log Out"));
                return logoutLink.Text.Contains("Log Out");
            }
            else if (IsChartViewer(application))
            {
                IWebElement logoutLink = GetLogoutLink(application);
                AppendToFile("Logout Link = " + logoutLink.Text);
                AppendToFile("Returned Value = " + logoutLink.Text.Contains("Log Out"));
                return logoutLink.Text.Contains("Log Out");
            }
            else
                return false;
        }

        internal static bool CheckOrg(string level)
        {
            AppendToFile("Level = " + level);
            string orgSelector = "";
            if (level.Equals("Both"))
                orgSelector = "selectedOrgId";
            else
                orgSelector = "orgSelector";

            IWebElement org = webDriver.FindElement(By.Id(orgSelector));

            if (level.Equals("Sleep") || level.Equals("Balance"))
                return org.Text.Contains("{" + level + "}"); // {Sleep} or {Balance}
            else if (level.Equals("Both"))
            {
                if (org.Text.Contains("Sleep") && org.Text.Contains("Balance"))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public static bool FindTile(string expectedTileGroup, string expectedTileName, string expectedTileID, string expectedTileType)
        {
            // Click the "eyeball" link at the top of the page.
            var control = webDriver.FindElement(By.Id("controlButton"));
            Browser.sleep(3);
            control.Click();
            Browser.sleep(3);

            // Scroll through all the Tiles until it is found.
            var tileRacks = webDriver.FindElements(By.ClassName("applicationTileRack"));
            bool found = false;
            String tileType = expectedTileType + "Tile";
            for (int i = 0; (i < tileRacks.Count && found == false); i++)
            {
                var tileRack = tileRacks[i];

                AppendToFile("Tile Rack = " + tileRack.Text);

                // tileType is either "applicationTile" or "managerTile".
                var tiles = tileRack.FindElements(By.ClassName(tileType));

                for (int j = 0; (j < tiles.Count && found == false); j++)
                {
                    var tile = tiles[j];

                    AppendToFile("Tile = " + tile.ToString());

                    String tileID = tiles[j].GetAttribute("tile-id");

                    AppendToFile("Tile ID = " + tileID);
                    AppendToFile("Expected Tile ID = " + expectedTileID);
                    AppendToFile("Expected Tile Group = " + expectedTileGroup);
                    AppendToFile("Expected Tile Name = " + expectedTileName);

                    if (tileID.Equals(expectedTileID))
                    {
                        var searchStudies = tile.FindElement(By.ClassName("applicationTileAnchor"));

                        AppendToFile("Tile contents: " + tile.Text);
                        AppendToFile("Contains Group: " + tile.Text.Contains(expectedTileGroup));
                        AppendToFile("Contains Name: " + tile.Text.Contains(expectedTileName));

                        found = (tile.Text.Contains(expectedTileGroup) && tile.Text.Contains(expectedTileName));
                        AppendToFile("Found = " + found);
                    }
                }
            }
            AppendToFile("Found = " + found);
            return found;
        }

        private static IWebElement GetLogoutLink(string application)
        {
            IWebElement logoutLink = null, logoutForm = null;
            if (IsProximus(application))
            {
                logoutForm = webDriver.FindElement(By.Id("logoutForm"));
                logoutLink = logoutForm.FindElement(By.TagName("a"));
            }
            else if (IsDoctorsApp(application))
            {
                logoutForm = webDriver.FindElement(By.Id("Banner1_BannerMessage"));
                logoutLink = logoutForm.FindElement(By.TagName("a"));
            }
            else if (IsChartViewer(application))
            {
                logoutForm = getChartViewerObject(logoutForm);
                logoutLink = logoutForm.FindElement(By.TagName("a"));
                AppendToFile("Log out Line = " + logoutLink.Text);
            }
            AppendToFile("Logout Link = " + logoutLink.Text);
            return logoutLink;
        }

        public static void sleep(int n)
        {
            System.Threading.Thread.Sleep(n * 1000);
        }

        public static void AppendToFile(string text)
        {
            string path = @"c:\temp\browseroutput.txt";

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(text);
            }
        }
    }
}