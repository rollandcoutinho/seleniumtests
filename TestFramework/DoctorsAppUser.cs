﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework
{
    public class DoctorsAppUser : TestUser
    {
        public static string accessDeniedMessage = "You don't have access to the Doctor's Application.";

        public DoctorsAppUser(string userRole, string userLevel)
        {
            this.UserRole = userRole;
            this.UserLevel = userLevel;
            this.Password = "Welcome1!";
            this.TabSelected = "";
            this.TabUnselected = "";
            this.Message = "";

            switch (userRole)
            {
                case "Personal Reading Doctor":
                    this.UserName = "dr-persread";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "Specialist Consult";
                    break;
                case "Practice Reading Doctor":
                    this.UserName = "dr-practread";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "Specialist Consult";
                    break;
                case "Personal Referring Doctor":
                    this.UserName = "dr-persrefer";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "To Be Read";
                    break;
                case "Practice Referring Doctor":
                    this.UserName = "dr-practrefer";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "To Be Read";
                    break;
                case "Reading Staff":
                    this.UserName = "dr-readstaff";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "Referred Studies";
                    break;
                case "Referring Staff":
                    this.UserName = "dr-referstaff";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "To Be Read";
                    break;
                case "Reset Password":
                    this.TabSelected = "Reset Password Page";
                    this.UserName = "dr-changeme";
                    this.Password = "changeme";
                    this.Message = "Enter Your New Sleepcare Password";
                    break;
                case "Chart Viewer User":
                    this.UserName = "dr-cvuser";
                    this.Message = accessDeniedMessage;
                    break;
                case "Chart Viewer Medical Records":
                    this.UserName = "dr-cvmedrec";
                    this.Message = accessDeniedMessage;
                    break;
                case "COS Doctor":
                    this.UserName = "dr-cosdoctor";
                    this.Message = accessDeniedMessage;
                    break;
                case "COS Hospital":
                    this.UserName = "dr-coshospital";
                    this.Message = accessDeniedMessage;
                    break;
                case "SCC Dictation Doctor":
                    this.UserName = "dr-sccdictation";
                    this.Message = accessDeniedMessage;
                    break;
                default:
                    this.UserName = "dr-persread";
                    this.TabSelected = "To Be Scheduled";
                    this.TabUnselected = "Specialist Consult";
                    break;
            }
        }

        public string TabSelected { get; set; }
        public string TabUnselected { get; set; }
        public string Message { get; set; }

        public string Convert2String()
        {
            StringBuilder sb = new System.Text.StringBuilder();

            if (this.UserName.Length > 0) sb.Append("UserName = " + this.UserName + "\n");
            if (this.Password.Length > 0) sb.Append("Password = " + this.Password + "\n");
            if (this.TabSelected.Length > 0) sb.Append("TabSelected = " + this.TabSelected + "\n");
            if (this.TabUnselected.Length > 0) sb.Append("TabUnselected = " + this.TabUnselected + "\n");
            if (this.Message.Length > 0) sb.Append("Message = " + this.Message + "\n");

            return sb.ToString();
        }
    }
}