﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework
{
    public static class Pages
    {
        public static class ChartViewerLoginPage
        {
            public static String Url = "https://devsleep.persante.com/chartviewer";
            public static String PageTitle = "Chart Viewer";
            public static String application = PageTitle;

            public static void Goto()
            {
                Browser.GoTo(Url);
            }

            public static bool IsAt()
            {
                return Browser.Title.Trim() == PageTitle;
            }

            public static bool Login(string userName, string password, bool expectedOutcome, string expectedMessage)
            {
                return Browser.Login(userName, password, expectedOutcome, expectedMessage, application);
            }

            public static bool LogoutExists()
            {
                Browser.sleep(5);
                return Browser.LogoutExists(application);
            }

            public static bool Logout()
            {
                return Browser.Logout(application);
            }
        }

        public static class ChartViewerHomePage
        {
            public static String Url = "https://devsleep.persante.com/chartviewer";
            public static String PageTitle = "Patient Search [Chart Viewer]";
            public static String application = "Chart Viewer";

            public static bool LogoutExists()
            {
                Browser.sleep(5);
                return Browser.LogoutExists(application);
            }

            public static bool Logout()
            {
                return Browser.Logout(application);
            }

            public static bool Login(string userName, string password, bool expectedOutcome, string expectedMessage)
            {
                return Browser.Login(userName, password, expectedOutcome, expectedMessage, application);
            }
        }

        public static class DoctorsAppHomePage
        {
            public static String Url = "https://devsleep.persante.com/doctorsapplication";
            public static String PageTitle = "Doctor Gui(To Be Scheduled)";
            public static String application = "Doctors App";

            public static bool LogoutExists()
            {
                Browser.sleep(5);
                return Browser.LogoutExists(application);
            }

            public static bool Logout()
            {
                return Browser.Logout(application);
            }
        }

        public static class DoctorsAppLoginPage
        {
            public static String Url = "https://devsleep.persante.com/doctorsapplication";
            public static String PageTitle = "Doctor Gui(Login)";
            public static string application = "Doctors App";

            public static void Goto()
            {
                Browser.GoTo(Url);
            }

            public static bool IsAt()
            {
                return Browser.Title == PageTitle;
            }

            public static bool Login(string userName, string password, bool expectedOutcome, string expectedMessage)
            {
                return Browser.Login(userName, password, expectedOutcome, expectedMessage, application);
            }

            // if just "userName" and "password" sent in, then expecting a successful login.  Not testing for it.
            public static bool Login(string userName, string password)
            {
                return Browser.Login(userName, password, true, "", application);
            }

            public static bool FindTab(string expectedTabName, string expectedTabSelection)
            {
                return Browser.FindTab(expectedTabName, expectedTabSelection);
            }

            public static bool AccessDenied()
            {
                return Browser.AccessDenied(application);
            }

            public static bool ResetPassword(string pageHeader, string subHeader)
            {
                return Browser.ResetPassword(pageHeader, subHeader, application);
            }
        }

        public static class ProximusHomePage
        {
            public static String Url = "https://devproximus.persante.com";
            public static String PageTitle = "- Proximus";
            public static string application = "Proximus";
            public static void Goto()
            {
                Browser.GoTo(Url);
            }

            public static bool IsAt()
            {
                // It could be either "Home - Proximus" or "Manage Account - Proximus".
                return Browser.Title.Contains(application);
            }

            public static bool LogoutExists()
            {
                Browser.sleep(5);
                return Browser.LogoutExists(application);

            }

            public static bool Logout()
            {
                return Browser.Logout(application);
            }
        }

        public static class ProximusLoginPage
        {
            public static String Url = "https://devproximus.persante.com";
            public static String PageTitle = "Log in - Persante";
            public static string application = "Proximus";

            public static void Goto()
            {
                Browser.GoTo(Url);
            }

            public static bool IsAt()
            {
                return Browser.Title == PageTitle;
            }

            public static bool Login(string userName, string password, bool expectedOutcome, string expectedMessage)
            {
                return Browser.Login(userName, password, expectedOutcome, expectedMessage, application);
            }

            // if just "userName" and "password" sent in, then expecting a successful login.  Not testing for it.
            public static bool Login(string userName, string password)
            {
                return Browser.Login(userName, password, true, "", application);
            }

            public static bool ResetPassword(string pageHeader, string subHeader)
            {
                return Browser.ResetPassword(pageHeader, subHeader, application);
            }

            public static bool CheckOrg(string level)
            {
                return Browser.CheckOrg(level);
            }

            public static bool FindTile(string expectedTileGroup, string expectedTileName, string expectedTileID)
            {
                return Browser.FindTile(expectedTileGroup, expectedTileName, expectedTileID, "application");
            }

            public static bool FindTile(string expectedTileGroup, string expectedTileName, string expectedTileID, string expectedTileType)
            {
                return Browser.FindTile(expectedTileGroup, expectedTileName, expectedTileID, expectedTileType);
            }
        }
    }
}
