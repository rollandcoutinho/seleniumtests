﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework
{
    public class ProximusTestUser : TestUser
    {
        public ProximusTestUser(string userRole, string userLevel)
        {
            this.UserRole = userRole;
            this.UserLevel = userLevel;

            switch (userRole)
            {
                case "Administrator":
                    this.UserName = "admintest";
                    this.Password = "Welcome1!";
                    this.TileGroup = "Administration";
                    this.TileName = "User Management";
                    this.TileLevel = "manager";
                    this.TileID = "33";
                    break;
                case "Scheduling":
                    switch (userLevel)
                    {
                        case "Sleep":
                            this.UserName = "sleeponlytest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "Org Selector";
                            this.TileName = "Sleep";
                            this.TileLevel = "";
                            this.TileID = "";
                            break;
                        case "Both":
                            this.UserName = "sleepbalancetest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "SelectedOrgId";
                            this.TileName = "Sleep";
                            this.TileLevel = "";
                            this.TileID = "";
                            break;
                        case "Balance":
                            this.UserName = "balanceonlytest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "Org Selector";
                            this.TileName = "Balance";
                            this.TileLevel = "";
                            this.TileID = "";
                            break;
                        case "Reset":
                            this.UserName = "resetpasstest";
                            this.Password = "changeme";
                            this.TileGroup = "";
                            this.TileName = "";
                            this.TileLevel = "";
                            this.TileID = "";
                            break;
                        case "User":
                            this.UserName = "scheduletest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "Scheduling";
                            this.TileName = "To Be Scheduled";
                            this.TileLevel = "application";
                            this.TileID = "12";
                            break;
                    }
                    break;
                case "Charts":
                    this.UserName = "chartstest";
                    this.Password = "Welcome1!";
                    this.TileGroup = "Medical Records";
                    this.TileName = "Incomplete Charts";
                    this.TileLevel = "application";
                    this.TileID = "7";
                    break;
                case "ExternalTech":
                case "Tech":
                    if (userRole.Equals("Tech"))
                        this.UserName = "techusertest";
                    else
                        this.UserName = "exttechtest";
                    this.Password = "Welcome1!";
                    this.TileGroup = "Sleep Center";
                    this.TileName = "TechGui";
                    this.TileLevel = "application";
                    this.TileID = "37";
                    break;
                case "Insurance":
                    this.UserName = "insuretest";
                    this.Password = "Welcome1!";
                    this.TileGroup = "Insurance";
                    this.TileName = "Verification";
                    this.TileLevel = "application";
                    this.TileID = "10";
                    break;
                case "Remote Office":
                    this.UserName = "remofficetest";
                    this.Password = "Welcome1!";
                    this.TileGroup = "General";
                    this.TileName = "Search Studies";
                    this.TileLevel = "application";
                    this.TileID = "5";
                    break;
                case "Scoring":
                    switch (userLevel)
                    {
                        case "User":
                            this.UserName = "scoreusertest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "General";
                            this.TileName = "Search Patients";
                            this.TileLevel = "application";
                            this.TileID = "4";
                            break;
                        case "Manager":
                            this.UserName = "scoremgrtest";
                            this.Password = "Welcome1!";
                            this.TileGroup = "Scoring";
                            this.TileName = "To Be Scored";
                            this.TileID = "20";
                            this.TileLevel = "manager";
                            break;
                    }
                    break;
                default:
                    this.Password = "Welcome1!";
                    this.TileGroup = "";
                    this.TileName = "";
                    this.TileLevel = "";
                    this.TileID = "";
                    break;
            }
        }
    }
}
