﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFramework
{
    public class TestUser : object
    {
        public TestUser()
        {
            this.UserRole = "";
            this.UserLevel = "";
        }
        public TestUser(string userRole, string userLevel)
        {
            this.UserRole = userRole;
            this.UserLevel = userLevel;
        }

        public TestUser(string userRole, string userLevel, string application)
        {
            TestUser user = null;
            if (Browser.IsProximus(application))
                user = new ProximusTestUser(userRole, userLevel);
            else if (Browser.IsDoctorsApp(application))
                user = new DoctorsAppUser(userRole, userLevel);
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string TileGroup { get; set; }
        public string TileName { get; set; }
        public string TileID { get; set; }
        public string TileLevel { get; set; }
        public string UserRole { get; set; }
        public string UserLevel { get; set; }

        public string convert2String
        {
            get
            {
                StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("UserName = " + this.UserName + "\n");
                sb.Append("Password = " + this.Password + "\n");
                sb.Append("TileGroup = " + this.TileGroup + "\n");
                sb.Append("TileName = " + this.TileName + "\n");
                sb.Append("TileID = " + this.TileID + "\n");
                sb.Append("TileLevel = " + this.TileLevel);

                return sb.ToString();
            }
        }
    }
}
