﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;

namespace Tests
{
    [TestClass]
    public class ChartViewerLoginTest
    {
        [TestMethod]
        public void setup()
        {
            ;
        }

        [TestMethod]
        public void AlwaysTrueTest()
        {
            Assert.IsTrue(true);
        }


        [TestMethod]
        public void CanReachChartViewerLoginPage()
        {
            Browser.getDriver();
            Pages.ChartViewerLoginPage.Goto();
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Browser.Close();
        }

        [TestMethod]
        public void ChartViewerLoginAndLogoutSucceeds()
        {
            Browser.getDriver();
            String userName = "charttest";
            String firstName = "chart";
            String lastName = "Test";
            String password = "Welcome1!";
            String greeting = "Welcome " + firstName + " " + lastName + "!";
            bool expectedLoginSucceeds = true;

            Pages.ChartViewerLoginPage.Goto();
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Assert.IsTrue(Pages.ChartViewerLoginPage.Login(userName, password, expectedLoginSucceeds, greeting));
            Assert.IsTrue(Pages.ChartViewerHomePage.LogoutExists());
            Assert.IsTrue(Pages.ChartViewerHomePage.Logout());
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Browser.Close();
        }

        [TestMethod]
        public void ChartViewerLoginFails4BadPassword()
        {
            Browser.getDriver();
            String userName = "dr-persread";
            String password = "xxx";
            String greeting = "Hello, " + userName + "!";
            String loginError = "Login Failed!";
            bool expectedLoginSucceeds = false;

            Pages.ChartViewerLoginPage.Goto();
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Assert.IsTrue(Pages.ChartViewerLoginPage.Login(userName, password, expectedLoginSucceeds, loginError));
            Browser.Close();
        }

        [TestMethod]
        public void ChartViewerLoginFails4NullPassword()
        {
            Browser.getDriver();
            String userName = "dr-persread";
            String greeting = "Hello, " + userName + "!";
            String loginError = "Login Failed!";
            bool expectedLoginSucceeds = false;

            Pages.ChartViewerLoginPage.Goto();
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Assert.IsTrue(Pages.ChartViewerLoginPage.Login(userName, "", expectedLoginSucceeds, loginError));
            Browser.Close();
        }

        [TestMethod]
        public void ChartViewerLoginFails4BadUserId()
        {
            Browser.getDriver();
            String userName = "xxx";
            String password = "Welcome1!";
            String greeting = "Hello, " + userName + "!";
            String loginError = "Login Failed!";
            bool expectedLoginSucceeds = false;

            Pages.ChartViewerLoginPage.Goto();
            Assert.IsTrue(Pages.ChartViewerLoginPage.IsAt());
            Assert.IsTrue(Pages.ChartViewerLoginPage.Login(userName, password, expectedLoginSucceeds, loginError));
            Browser.Close();
        }

        [TestCleanup]
        public void CleanUp()
        {
            ;
            //Browser.Close();
        }
    }
}
