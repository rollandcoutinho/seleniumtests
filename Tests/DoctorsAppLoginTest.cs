﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;

namespace Tests
{
    [TestClass]
    public class DoctorsAppLoginTest
    {
        [TestMethod]
        public void AlwaysTrueTest()
        {
            Assert.IsTrue(true);
        }


        [TestMethod]
        public void CanReachDoctorsAppLoginPage()
        {
            Browser.getDriver();

            Pages.DoctorsAppLoginPage.Goto();
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());

            Browser.Close();
        }

        [TestMethod]
        public void DoctorsAppLoginAndLogoutSucceeds()
        {
            Browser.getDriver();

            String userName = "dr-persread";
            String lastName = "Test";
            String password = "Welcome1!";
            String greeting = "Welcome " + userName + " " + lastName + "!";
            bool expectedLoginSucceeds = true;

            Pages.DoctorsAppLoginPage.Goto();
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());
            Assert.IsTrue(Pages.DoctorsAppLoginPage.Login(userName, password, expectedLoginSucceeds, greeting));
            Assert.IsTrue(Pages.DoctorsAppHomePage.LogoutExists());
            Assert.IsTrue(Pages.DoctorsAppHomePage.Logout());
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());

            Browser.Close();
        }

        [TestMethod]
        public void DoctorsAppLoginFails4BadPassword()
        {
            Browser.getDriver();

            String userName = "dr-persread";
            String password = "xxx";
            String greeting = "Hello, " + userName + "!";
            String loginError = "Login Failed!";
            bool expectedLoginSucceeds = false;

            Pages.DoctorsAppLoginPage.Goto();
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());
            Assert.IsTrue(Pages.DoctorsAppLoginPage.Login(userName, password, expectedLoginSucceeds, loginError));

            Browser.Close();
        }

        [TestMethod]
        public void DoctorsAppLoginFails4NullPassword()
        {
            Browser.getDriver();

            String userName = "dr-persread";
            String greeting = "Hello, " + userName + "!";
            String loginError = "User Name and Password are required.";
            bool expectedLoginSucceeds = false;

            Pages.DoctorsAppLoginPage.Goto();
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());
            Assert.IsTrue(Pages.DoctorsAppLoginPage.Login(userName, "", expectedLoginSucceeds, loginError));

            Browser.Close();
        }

        [TestCleanup]
        public void CleanUp()
        {
            ;
            //Browser.Close();
        }
    }
}
