﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;
using System.IO;

namespace Tests
{
    [TestClass]
    public class DoctorsAppTabTest
    {
        [TestMethod]
        public void AlwaysTrueTest()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void DrAppCheck4ResetPassword()
        {
            string role = "Reset Password";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppPersonalReadingDoctor()
        {
            string role = "Personal Reading Doctor";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppPracticeReadingDoctor()
        {
            string role = "Practice Reading Doctor";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppPersonalReferringDoctor()
        {
            string role = "Personal Referring Doctor";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppPracticeReferringDoctor()
        {
            string role = "Practice Referring Doctor";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppReadingStaff()
        {
            string role = "Reading Staff";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppReferringStaff()
        {
            string role = "Referring Staff";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppChartViewerUser()
        {
            string role = "Chart Viewer User";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppChartViewerMedicalRecordsUser()
        {
            string role = "Chart Viewer Medical Records";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppCOSDoctorUser()
        {
            string role = "COS Doctor";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void DrAppCOSHospitalUser()
        {
            string role = "COS Hospital";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        private static void VerifyUserTileAccess(string userName, string level)
        {
            Browser.getDriver();

            var application = Pages.DoctorsAppLoginPage.application;
            DoctorsAppUser user = new DoctorsAppUser(userName, level);

            Pages.DoctorsAppLoginPage.Goto();
            Browser.sleep(2);
            Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());

            AppendToFile("User Name = " + userName + "; Message = " + user.Message);
            if (user.Message.Equals(DoctorsAppUser.accessDeniedMessage))

            {
                Assert.IsTrue(Pages.DoctorsAppLoginPage.Login(user.UserName, user.Password, false, user.Message));

                AppendToFile("Checking for Access Denied...");
                Assert.IsTrue(Pages.DoctorsAppLoginPage.AccessDenied());
            }
            else
            {
                Assert.IsTrue(Pages.DoctorsAppLoginPage.Login(user.UserName, user.Password));
                if (!user.UserRole.Equals("Reset Password"))
                {
                    AppendToFile("Checking the login...");
                    Assert.IsTrue(Pages.DoctorsAppLoginPage.FindTab(user.TabSelected, "Selected"));
                    Assert.IsTrue(Pages.DoctorsAppLoginPage.FindTab(user.TabUnselected, "Unselected"));
                }
                else if (user.UserRole.Equals("Reset Password"))
                {
                    string pageHeader = user.Message;
                    AppendToFile("Resetting the password...");
                    Assert.IsTrue(Pages.DoctorsAppLoginPage.ResetPassword(pageHeader, ""));
                }
                Assert.IsTrue(Pages.DoctorsAppHomePage.LogoutExists());
                Assert.IsTrue(Pages.DoctorsAppHomePage.Logout());
                Assert.IsTrue(Pages.DoctorsAppLoginPage.IsAt());
            }

            Browser.Close();
        }


        public static void AppendToFile(string text)
        {
            string path = @"c:\temp\browseroutput.txt";

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(text);
            }
        }


        [TestCleanup]
        public void CleanUp()
        {
            ;
            //Browser.Close();
        }
    }

}
