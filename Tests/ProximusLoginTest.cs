﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;
using System.IO;

namespace Tests
{
    [TestClass]
    public class ProximusLoginTest
    {
        [TestMethod]
        public void CanReachProximusLoginPage()
        {
            Browser.getDriver();

            Pages.ProximusLoginPage.Goto();
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());

            Browser.Close();
        }

        [TestMethod]
        public void ProximusLoginAndLogoutSucceeds()
        {
            Browser.getDriver();

            String userName = "scheduletest";
            String password = "Welcome1!";
            String greeting = "Hello, " + userName + "!";
            bool expectedLoginSucceeds = true;

            Pages.ProximusLoginPage.Goto();
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());
            /* Login(string userNameValue, string passwordValue, string application, bool expectedOutcome, string expectedMessage) */
            Assert.IsTrue(Pages.ProximusLoginPage.Login(userName, password, expectedLoginSucceeds, greeting));
            Assert.IsTrue(Pages.ProximusHomePage.LogoutExists());
            Assert.IsTrue(Pages.ProximusHomePage.Logout());
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());

            Browser.Close();
        }

        [TestMethod]
        public void ProximusLoginFails4BadPassword()
        {
            Browser.getDriver();

            String userName = "scheduletest";
            String password = "xxx";
            String greeting = "Hello, " + userName + "!";
            String loginError = "The user name or password provided is incorrect.";
            bool expectedLoginSucceeds = false;

            Pages.ProximusLoginPage.Goto();
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());
            Assert.IsTrue(Pages.ProximusLoginPage.Login(userName, password, expectedLoginSucceeds, loginError));

            Browser.Close();
        }

        [TestMethod]
        public void ProximusLoginFails4NullPassword()
        {
            Browser.getDriver();

            String userName = "scheduletest";
            String password = "";
            String greeting = "Hello, " + userName + "!";
            String loginError = "The user name or password provided is incorrect.";
            bool expectedLoginSucceeds = false;


            Pages.ProximusLoginPage.Goto();
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());
            Assert.IsTrue(Pages.ProximusLoginPage.Login(userName, password, expectedLoginSucceeds, loginError));

            Browser.Close();
        }

        [TestCleanup]
        public void CleanUp()
        {
            ;
            //Browser.Close();
        }
    }
}
