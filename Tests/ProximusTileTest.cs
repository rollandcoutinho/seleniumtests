﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestFramework;
using System.IO;

namespace Tests
{
    [TestClass]
    public class ProximusTileTest
    {
        [TestMethod]
        public void AlwaysTrueTest()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void ProximusCheck4ResetPassword()
        {
            string role = "Scheduling";
            string level = "Reset";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusCheck4OrgSleep()
        {
            string role = "Scheduling";
            string level = "Sleep";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusCheck4OrgSleepAndBalance()
        {
            string role = "Scheduling";
            string level = "Both";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusCheck4OrgBalance()
        {
            string role = "Scheduling";
            string level = "Balance";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4AdminUser()
        {
            string role = "Administrator";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4ChartsUser()
        {
            string role = "Charts";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4InsuranceUser()
        {
            string role = "Insurance";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4RemoteOfficeUser()
        {
            string role = "Remote Office";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4SchedulingUser()
        {
            string role = "Scheduling";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4ExternalTechUser()
        {
            string role = "ExternalTech";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4TechUser()
        {
            string role = "Tech";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4ScoringUser()
        {
            string role = "Scoring";
            string level = "User";
            VerifyUserTileAccess(role, level);
        }

        [TestMethod]
        public void ProximusTile4ScoringManager()
        {
            string role = "Scoring";
            string level = "Manager";
            VerifyUserTileAccess(role, level);
        }

        private static void VerifyUserTileAccess(string userName, string level)
        {
            Browser.getDriver();

            var application = Pages.ProximusLoginPage.application;
            ProximusTestUser user = new ProximusTestUser(userName, level);

            if (userName.Equals("Scoring"))
                AppendToFile(user.convert2String);

            Pages.ProximusLoginPage.Goto();
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());
            Assert.IsTrue(Pages.ProximusLoginPage.Login(user.UserName, user.Password));
            if (level.Equals("Sleep") || level.Equals("Both") || level.Equals("Balance"))
            {
                AppendToFile("Checking the org...");
                Assert.IsTrue(Pages.ProximusLoginPage.CheckOrg(level));
            }
            else if (!level.Equals("Reset"))
            {
                AppendToFile("Checking the login...");
                Assert.IsTrue(Pages.ProximusLoginPage.FindTile(user.TileGroup, user.TileName, user.TileID, user.TileLevel));
            }
            else if (level.Equals("Reset"))
            {
                string pageHeader = "Manage Account";
                string subHeader = "Change password";
                AppendToFile("Resetting the password...");
                Assert.IsTrue(Pages.ProximusLoginPage.ResetPassword(pageHeader, subHeader));
            }
            Assert.IsTrue(Pages.ProximusHomePage.LogoutExists());
            Assert.IsTrue(Pages.ProximusHomePage.Logout());
            Assert.IsTrue(Pages.ProximusLoginPage.IsAt());

            Browser.Close();
        }


        public static void AppendToFile(string text)
        {
            string path = @"c:\temp\ProximusTileTestOutput.txt";

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(text);
            }
        }


        [TestCleanup]
        public void CleanUp()
        {
            ;
            //Browser.Close();
        }
    }

}
