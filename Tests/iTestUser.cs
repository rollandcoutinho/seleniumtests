﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public interface ITestUser : IEnumerable
    {
        string UserName { get; set; }
        string Password { get; set; }
        string TileGroup { get; set; }
        string TileName { get; set; }
        string TileID { get; set; }
        string TileLevel { get; set; }
    }
}
