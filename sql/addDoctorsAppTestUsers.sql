/*
UserID/1st Name	Last 	Level	Role1	Role2 	Role 1				Org 1	Level 1	Role 2		Org 2	Level 2
dr-persread		Test	User	24				Personal Read Dr	Sleep	User			
dr-practread	Test	User	22				Practice Read Dr	Sleep	User			
dr-persrefer	Test	User	25				Personal Refer Dr	Sleep	User			
dr-practrefer	Test	User	23				Practice Refer Dr	Sleep	User			
dr-readstaff	Test	User	19				Read Staff			Sleep	User			
dr-referstaff	Test	User	20				Refer Staff			Sleep	User			
dr-cvuser		Test	User	18				CV User				Sleep	User			
dr-cvmedrec		Test	User	91		18		CV Medical Records	Sleep	User	CV User		Sleep	User
dr-cosdoctor	Test	User	34		18		COS Doctor			Sleep	User	CV User		Sleep	User
dr-coshospital	Test	User	33		18		COS Hospital		Sleep	User	CV User		Sleep	User
dr-sccdictation	Test	User	30		18		SCC Dictation Dr	Sleep	User	CV User		Sleep	User
dr-changeme		Test	User	24		18		Personal Read Dr	Sleep	User		
 */

DECLARE @UserID    as nvarchar(20)
DECLARE @FirstName as nvarchar(20)
DECLARE @LastName  as nvarchar(10)
DECLARE @Password  as nvarchar(10)
DECLARE @Role1 as int
DECLARE @Role2 as int
DECLARE @UserLevel as nvarchar(10)
DECLARE @UserCount as int
DECLARE @UserLevelValue as int
DECLARE @RoleCount as int

SET @LastName = 'Test';
SET @Password = 'Welcome1!';
SET @UserLevel = 'User';


-- #1 - Personal Reading Doctor:
SET @UserID = 'dr-persread';
SET @FirstName = @UserID;
SET @Role1 = 24;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);

   
   
-- #1 - Practice Reading Doctor:
SET @UserID = 'dr-practread';
SET @FirstName = @UserID;
SET @Role1 = 22;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);   
   


-- #3 - Personal Referring Doctor:
SET @UserID = 'dr-persrefer';
SET @FirstName = @UserID;
SET @Role1 = 25;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);



-- #4 - Personal Reading Doctor:
SET @UserID = 'dr-practrefer';
SET @FirstName = @UserID;
SET @Role1 = 23;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #5 - Reading Staff:
SET @UserID = 'dr-readstaff';
SET @FirstName = @UserID;
SET @Role1 = 19;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);   




-- #6 - Referring Staff:
SET @UserID = 'dr-referstaff';
SET @FirstName = @UserID;
SET @Role1 = 19;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   
   


-- #7 - Chart Viewer User:
SET @UserID = 'dr-cvuser';
SET @FirstName = @UserID;
SET @Role1 = 18;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #8 - Chart Viewer Medical Records:
SET @UserID = 'dr-cvmedrec';
SET @FirstName = @UserID;
SET @Role1 = 91;
SET @Role2 = 18;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #9 COS Doctor:
SET @UserID = 'dr-cosdoctor';
SET @FirstName = @UserID;
SET @Role1 = 34;
SET @Role2 = 18;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #10 - COS Hospital:
SET @UserID = 'dr-coshospital';
SET @FirstName = @UserID;
SET @Role1 = 33;
SET @Role2 = 18;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #11 - SCC Dictation Doctor:
SET @UserID = 'dr-sccdictation';
SET @FirstName = @UserID;
SET @Role1 = 30;
SET @Role2 = 18;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);




-- #12 - Change Password Test User:
SET @UserID = 'dr-changeme';
SET @FirstName = @UserID;
SET @Role1 = 24;
SET @Role2 = 18;
SET @Password = 'changeme';

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);

   
GO


SELECT *
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'dr-persread',	
'dr-practread',	
'dr-persrefer',
'dr-practrefer',
'dr-readstaff',
'dr-referstaff',
'dr-cvuser',
'dr-cvmedrec',	
'dr-cosdoctor',
'dr-coshospital',
'dr-sccdictation',
'dr-changeme'
)
 and (Last_Name = 'Test'));
 
GO
