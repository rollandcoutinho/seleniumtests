/*
User ID			First Name		Last 	Password	Level	Role1	Role2	Role 1			Org 1	Level 1	Role 2			Org 2	Level 2
admintest		Admin			Test	Welcome1!	User	36		73		Administrator	Sleep	User	Administrator	Balance	User
chartstest		Charts			Test	Welcome1!	User	3		0		Charts			Sleep	User			
resetpasstest	ResetPassword	Test	changeme	User	1		0		Scheduling		Sleep	User			
exttechtest		ExtTech			Test	Welcome1!	User	95		0		External Tech	Sleep	User			
insuretest		Insurance		Test	Welcome1!	User	2		0		Insurance		Sleep	User			
remofficetest	RemoteOffice	Test	Welcome1!	User	102		103		Remote Office	Sleep	User	Remote Office	Balance	User
schedtuletest	Schedule		Test	Welcome1!	User	1		0		Scheduling		Sleep	User			
sleeponlytest	Sleep-Only		Test	Welcome1!	User	1		0		Scheduling		Sleep	User			
sleepbalancetest Sleep-Balance	Test	Welcome1!	User	1		42		Scheduling		Sleep	User	Scheduling		Balance	User
balanceonlytest	Balance-Only	Test	Welcome1!	User	42		0		Scheduling		Balance	User			
scoreusertest	Score-User		Test	Welcome1!	User	5		46		Scoring			Sleep	User	Validation		Sleep	User
scoremgrtest	Score-Mgr		Test	Welcome1!	Manager	5		46		Scoring			Sleep	Manager	Validation		Sleep	Manager
techusertest	Tech			Test	Welcome1!	User	8		0		Tech			Sleep	User			
 */

DECLARE @UserID    as nvarchar(20)
DECLARE @FirstName as nvarchar(20)
DECLARE @LastName  as nvarchar(10)
DECLARE @Password  as nvarchar(10)
DECLARE @Role1 as int
DECLARE @Role2 as int
DECLARE @UserLevel as nvarchar(10)
DECLARE @UserCount as int
DECLARE @UserLevelValue as int
DECLARE @RoleCount as int

SET @LastName = 'Test';
SET @Password = 'Welcome1!';
SET @UserLevel = 'User';


-- #1 - ADMINTEST:
SET @UserID = 'admintest';
SET @FirstName = 'Admin';
SET @Role1 = 36;
SET @Role2 = 73;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   



-- #2 - CHARTSTEST:
SET @UserID = 'chartstest';
SET @FirstName = 'Charts';
SET @Role1 = 3;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   



-- #3 - RESETPASSTEST:
SET @UserID = 'resetpasstest';
SET @FirstName = 'Reset';
SET @Password = 'changeme';
SET @Role1 = 1;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #4 - EXTTECHTEST:
SET @UserID = 'exttechtest';
SET @FirstName = 'ExtTech';
SET @Password = 'Welcome1!';
SET @Role1 = 95;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #5 - INSURETEST:
SET @UserID = 'insuretest';
SET @FirstName = 'Insurance';
SET @Role1 = 2;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #6 - REMOFFICETEST:
SET @UserID = 'remofficetest';
SET @FirstName = 'RemoteOffice';
SET @Role1 = 102;
SET @Role2 = 103;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #7 - SCHEDULETEST:
SET @UserID = 'scheduletest';
SET @FirstName = 'Schedule';
SET @Role1 = 1;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #8 - SLEEPONLYTEST:
SET @UserID = 'sleeponlytest';
SET @FirstName = 'Sleep-Only';
SET @Role1 = 1;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #9 - SLEEPBALANCETEST:
SET @UserID = 'sleepbalancetest';
SET @FirstName = 'Sleep-Balance';
SET @Role1 = 1;
SET @Role2 = 42;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   



-- #10 - BALANCEONLYTEST:
SET @UserID = 'balanceonlytest';
SET @FirstName = 'Balance-Only';
SET @Role1 = 42;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 2);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 2
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #11 - SCOREUSERTEST:
SET @UserID = 'scoreusertest';
SET @FirstName = 'Score-User';
SET @Role1 = 5;
SET @Role2 = 13;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, 1, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, 1, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #12 - SCOREMGRTEST:
SET @UserID = 'scoremgrtest';
SET @FirstName = 'Score-Manager';
SET @Role1 = 5;
SET @Role2 = 13;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, @UserLevelValue, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, @UserLevelValue, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   




-- #13 - TECHUSERTEST:
SET @UserID = 'techusertest';
SET @FirstName = 'Tech';
SET @Role1 = 8;
SET @Role2 = 0;

-- Create/Update the User ID (tbl_Users)
SELECT @UserCount = count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID = @UserID);

IF (@UserLevel = 'User')
    SET @UserLevelValue = 1;
ELSE
    SET @UserLevelValue = 2;


IF (@UserCount = 0)
BEGIN
insert into tbl_Users (ID, Last_Name, First_Name, [Name], [Password], [Level], Dept, [Active], Global_User, OrgIdDefault) 
   values (@UserID, @LastName, @FirstName, @FirstName + ' ' + @LastName, @Password, @UserLevelValue, @Role1, 1, 1, 1);
END
ELSE IF (@UserCount > 0)
BEGIN
update tbl_Users set 
   Last_Name    = @LastName, 
   First_Name   = @FirstName, 
   Name         = @FirstName + ' ' + @LastName, 
   [Password]   = @Password, 
   [Level]      = @UserLevelValue, 
   Dept         = @Role1, 
   Active       = 1, 
   Global_User  = 1, 
   OrgIdDefault = 1
where ID = @UserID;
END


-- Add User Roles if they do not exist already (tbl_User_Roles)
SELECT @RoleCount = count(*)
FROM [scc1].[dbo].[tbl_User_Roles] 
WHERE  (UserID = @UserID);

IF (@RoleCount = 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default])
   values (@UserID, @UserLevelValue, @Role1, 1);

IF (@Role2 > 0)
insert into tbl_User_Roles (UserID, [Level], Dept, [Default]) 
   values (@UserID, @UserLevelValue, @Role2, 1);



-- Add Passwords regardless (User_Passwords)
-- Deactivate previous
update User_Passwords 
set    [Active] = 0 
where  [User_ID] = (select IdentityID from tbl_Users where ID = @UserID)
and    [Active] = 1;

-- Activate Sleep Password
insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
   values ((select IdentityID from tbl_Users where ID = @UserID), 1, @Password, getdate(), 1800, 1);

-- Activate Balance Password
--insert into User_Passwords (User_ID, Organization_ID, Password, AddedDate, AddedBy, Active) 
--   values ((select IdentityID from tbl_Users where ID = @UserID), 2, @Password, getdate(), 1800, 1);
   
GO




SELECT *
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'admintest',
'chartstest',
'resetpasstest',
'exttechtest',
'insuretest',
'remofficetest',
'scheduletest',
'sleeponlytest',
'sleepbaltest',
'balanceonlytest',
'scoreusertest',
'scoremgrtest',
'techusertest')
 and (Last_Name = 'Test'));
 
GO