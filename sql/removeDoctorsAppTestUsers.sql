SELECT count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'dr-persread',	
'dr-practread',	
'dr-persrefer',
'dr-practrefer',
'dr-readstaff',
'dr-referstaff',
'dr-cvuser',
'dr-cvmedrec',	
'dr-cosdoctor',
'dr-coshospital',
'dr-sccdictation',
'dr-changeme'
)
and (Last_Name = 'Test'));

-- Inactivate the User ID (tbl_Users)
update tbl_Users set Active = 0
WHERE  (ID in (
'dr-persread',	
'dr-practread',	
'dr-persrefer',
'dr-practrefer',
'dr-readstaff',
'dr-referstaff',
'dr-cvuser',
'dr-cvmedrec',	
'dr-cosdoctor',
'dr-coshospital',
'dr-sccdictation',
'dr-changeme'
)
and (Last_Name = 'Test'));


-- Delete the User Roles (tbl_User_Roles)
DELETE FROM tbl_User_Roles
WHERE  (UserID in (
'dr-persread',	
'dr-practread',	
'dr-persrefer',
'dr-practrefer',
'dr-readstaff',
'dr-referstaff',
'dr-cvuser',
'dr-cvmedrec',	
'dr-cosdoctor',
'dr-coshospital',
'dr-sccdictation',
'dr-changeme'
)
);


-- Inactivate all Passwords associated with this User ID (User_Passwords)
UPDATE User_Passwords 
SET    [Active] = 0 
WHERE  [User_ID] in (SELECT IdentityID 
                     FROM   tbl_Users 
					 WHERE  (ID in (
									'dr-persread',	
									'dr-practread',	
									'dr-persrefer',
									'dr-practrefer',
									'dr-readstaff',
									'dr-referstaff',
									'dr-cvuser',
									'dr-cvmedrec',	
									'dr-cosdoctor',
									'dr-coshospital',
									'dr-sccdictation',
									'dr-changeme'
									)) 
					and (Last_Name = 'Test'))
AND    [Active] = 1;


SELECT *
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'dr-persread',	
'dr-practread',	
'dr-persrefer',
'dr-practrefer',
'dr-readstaff',
'dr-referstaff',
'dr-cvuser',
'dr-cvmedrec',	
'dr-cosdoctor',
'dr-coshospital',
'dr-sccdictation',
'dr-changeme'
)
 and (Last_Name = 'Test'));
