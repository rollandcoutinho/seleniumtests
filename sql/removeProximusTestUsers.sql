SELECT count(ID)
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'admintest',
'chartstest',
'resetpasstest',
'exttechtest',
'insuretest',
'remofficetest',
'scheduletest',
'sleeponlytest',
'sleepbalancetest',
'balanceonlytest',
'scoreusertest',
'scoremgrtest',
'techusertest')
and (Last_Name = 'Test'));

-- Inactivate the User ID (tbl_Users)
update tbl_Users set Active = 0
WHERE  (ID in (
'admintest',
'chartstest',
'resetpasstest',
'exttechtest',
'insuretest',
'remofficetest',
'scheduletest',
'sleeponlytest',
'sleepbalancetest',
'balanceonlytest',
'scoreusertest',
'scoremgrtest',
'techusertest')
and (Last_Name = 'Test'));


-- Delete the User Roles (tbl_User_Roles)
DELETE FROM tbl_User_Roles
WHERE  (UserID in (
'admintest',
'chartstest',
'resetpasstest',
'exttechtest',
'insuretest',
'remofficetest',
'scheduletest',
'sleeponlytest',
'sleepbalancetest',
'balanceonlytest',
'scoreusertest',
'scoremgrtest',
'techusertest'));


-- Inactivate all Passwords associated with this User ID (User_Passwords)
UPDATE User_Passwords 
SET    [Active] = 0 
WHERE  [User_ID] in (SELECT IdentityID 
                     FROM   tbl_Users 
					 WHERE  (ID in (
								'admintest',
								'chartstest',
								'resetpasstest',
								'exttechtest',
								'insuretest',
								'remofficetest',
								'scheduletest',
								'sleeponlytest',
								'sleepbalancetest',
								'balanceonlytest',
								'scoreusertest',
								'scoremgrtest',
								'techusertest')					 
					and (Last_Name = 'Test')))
AND    [Active] = 1;


SELECT *
FROM [scc1].[dbo].[tbl_Users] 
WHERE  (ID in (
'admintest',
'chartstest',
'resetpasstest',
'exttechtest',
'insuretest',
'remofficetest',
'scheduletest',
'sleeponlytest',
'sleepbalancetest',
'balanceonlytest',
'scoreusertest',
'scoremgrtest',
'techusertest')
 and (Last_Name = 'Test'));
